import {useEffect} from 'react';
import {useStateValue} from '../state';
import fetchService from './fetchService';
import config from '../config';
import actions from '../state/actions';

/**
 * Facade to kick off fetching remote data (`now_playing`, and `movie_genres` apis),
 * and dispatch result to the store
 * @param props
 * @returns {null}
 * @constructor
 */
const FetchAPI = (props) => {
  // *** hanging comma due to not requiring any data from the store,
  // *** but we do want the dispatch method
  const [, dispatch] = useStateValue();
  
  // *** ~onComponentDidMount / called on component ready
  useEffect(() => {
    getData(); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props]);
  
  /**
   * Fire concurrent api fetches
   */
  const getData = () => {
    // *** now playing
    fetchService.call(
        buildRequest(config.api.requests.now_playing.path),
        (result) => {
          dispatchData({
            status: result.status,
            type: actions.SET_NOW_PLAYING_DATA,
            payload: result.payload
          })
        }
    );
  
    // *** movie genres
    fetchService.call(
        buildRequest(config.api.requests.movie_genres.path),
        (result) => {
          dispatchData({
            status: result.status,
            type: actions.SET_MOVIE_GENRES_DATA,
            payload: result.payload
          })
        }
    );
  }
  
  /**
   * Quasi defensive facade
   * @param vo {Object} : 'value object'
   */
  const dispatchData = (vo) => {
    if (vo.status === 'ok') {
      dispatch(vo)
    } else {
      console.error('/FetchAPI/ -dispatchData ERROR', vo);
    }
  }
  
  /**
   * format url
   * @param request {String}
   * @returns {string}
   */
  const buildRequest = (request) => {
    return `${config.api.origin}/${request}?api_key=${config.api.key}&language=en-US&page=1`;
  }
  
  return null;
}

export default FetchAPI;
