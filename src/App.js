import React from 'react';
import {StateProvider} from './state';
import store from './state/store';
import FetchAPI from './services/FetchAPI';
import ResultsView from './components/resultsView/ResultsView';
import './scss/App.scss';
import Toolbar from './components/toolbar/Toolbar';

/**
 * Entry point.
 * Note <StateProvider> provides state management to all children
 * <FetchAPI> abstracts fetching data
 * @returns {*}
 * @constructor
 */
const App = () => {
  
  return (
      <StateProvider initialState={store.initialState} reducer={store.reducer}>
        <FetchAPI/>
        <Toolbar/>
        <ResultsView/>
      </StateProvider>
  )
}

export default App;
