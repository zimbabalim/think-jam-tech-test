
import actions from './actions';
import config from '../config';
import utils from './utils';

/**
 * Data store and reducer, processing offloaded to `utils`
 */
const store = {
  initialState: {
    nowPlayingData: null, // *** loaded from api
    movieGenresData: null, // *** loaded from api
    availableGenres: null, // *** scraped from `nowPlayingData`
    filteredResults: null, // *** output will be rendered in <ResultsView>
    genreSelections: [], // *** set via <GenreSelector>
    currVoteSelection: config.defaults.voteSelection.defaultValue // *** set via <VoteAverageSelector> initialised with default value from config
  },
  
  reducer: (state, action) => {
    
    switch (action.type) {
        
        // *** set 'now playing' data, parse and set `filteredResults` and `availableGenres` from result
      case actions.SET_NOW_PLAYING_DATA:
        const nowPlayingData = utils.parseNowPlayingData(action.payload).data;
        
        return {
          ...state,
          nowPlayingData,
          filteredResults: utils.filter(state.genreSelections, state.currVoteSelection, nowPlayingData),
          availableGenres: utils.parseNowPlayingData(action.payload).availableGenres
        };
        
        // *** parse genres data
      case actions.SET_MOVIE_GENRES_DATA:
        const movieGenresData = utils.parseMovieGenresData(action.payload);
        return {
          ...state,
          movieGenresData
        };
        
        // *** on genre selection changed process new `filteredResults` and store genre payload
      case actions.GENRE_SELECTION_CHANGED:
        return {
          ...state,
          filteredResults: utils.filter(action.payload, state.currVoteSelection, state.nowPlayingData),
          genreSelections: action.payload,
        };
        
        // *** on vote selection changed process new `filteredResults` and store vote payload
      case actions.VOTE_SELECTION_CHANGED:
        return {
          ...state,
          filteredResults: utils.filter(state.genreSelections, action.payload, state.nowPlayingData),
          currVoteSelection: action.payload
        };
      
      default:
        return state;
    }
  }
};

export default store;
