/**
 * Pure functional methods to process reducer requirements
 */
const utils = {
  
  /**
   * Refines raw 'now playing' data,
   * scrapes actual genres from items within via utility function.
   * @param data {Object}
   * @return {Object}
   */
  parseNowPlayingData: (data) => {
    return {
      data: data.results,
      availableGenres: utils.scrapeAvailableGenres(data.results)
    }
  },
  
  /**
   * Refine 'movie genres' data
   * This data ultimately acts as a lookup table
   * @param data {Object}
   */
  parseMovieGenresData: (data) => {
    return data.genres;
  },
  
  /**
   * Get all declared genres (as ids {Int}) from source
   * @param data {Array}
   * @return {Array}
   */
  scrapeAvailableGenres: (data) => {
    let temp = data.map((item) => {
      return item.genre_ids;
    });
    
    return [...new Set(temp.flat())]; // *** flat and unique'd
  },
  
  /**
   * Simple numeric sorting function
   * @param key {String}
   * @param data {Array}
   * @return {Array}
   */
  sortByNumericProperty: (key, data) => {
    return data.sort((a, b) => {
      return (a[key] < b[key]) ? 1 : -1;
    });
  },
  
  /**
   * Composite function that will be called on every user initiated update,
   * i.e. genre selection, vote selection,
   * as well as on initialisation with default inputs.
   * Finalises by ensuring results are ordered by popularity property
   * @param genres {Array}
   * @param votes {Array}
   * @param data {Array}
   * @return {Array}
   */
  filter: (genres, votes, data) => {
    const results = utils.filterItemsByVoteAverage(
        votes,
        utils.filterItemsByGenre(genres, data)
    );
    return utils.sortByNumericProperty('popularity', results);
  },
  
  /**
   * Retrieves items from data source that match all genre selections
   * @param ids {Array}
   * @param data {Array}
   * @return {Array}
   */
  filterItemsByGenre: (ids, data) => {
    
    const numIds = ids.length;
    
    if (numIds === 0) { // *** if no selections just return everything
      return data;
    }
    
    return data.reduce(
        (result, item) => {
          
          let matches = 0;
          
          ids.map((id) => { // *** each input id
            item.genre_ids.filter((genreId) => {
              if (id === genreId) {
                matches ++;
              }
              return null;
            });
            return null;
          });
          
          // *** if number of matches is equal to number of inputs we have a result
          if (matches === numIds) {
            return result.concat(item);
          }
          
          return result;
        }, []
    );
  },
  
  /**
   * Returns items whose `vote_average` value is >= `value`
   * @param value {Number}
   * @param data {Array}
   * @return {Array}
   */
  filterItemsByVoteAverage: (value, data) => {
    return data.reduce(
        (result, item) => {
          if (parseInt(item.vote_average, 10) >= parseInt(value, 10)) {
            return result.concat(item);
          }
          return result;
        }, []
    );
  }
}

export default utils;
