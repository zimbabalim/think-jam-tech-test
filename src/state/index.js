import React, {createContext, useContext, useReducer} from 'react';

/**
 * Pinched this from here https://medium.com/simply/state-management-with-react-hooks-and-context-api-at-10-lines-of-code-baf6be8302c
 * I've used this a few times for smaller projects instead of redux, it works well with most of the benefits
 */
export const StateContext = createContext();
export const StateProvider = ({reducer, initialState, children}) =>(
    <StateContext.Provider value={useReducer(reducer, initialState)}>
      {children}
    </StateContext.Provider>
);
export const useStateValue = () => useContext(StateContext);
