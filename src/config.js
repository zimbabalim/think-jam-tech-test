/**
 * Global configuration properties
 */
const config = {
  // *** Api properties
  api: {
    origin: 'https://api.themoviedb.org',
    key: '91a38b48bb065642a39988c51e4ba2c9',
    token: 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5MWEzOGI0OGJiMDY1NjQyYTM5OTg4YzUxZTRiYTJjOSIsInN1YiI6IjVlOTFiYjNhMzdiM2E5MDAxNThiOWQyZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.WCIrO4OJgfOGMCa5iTxOjysZL3eMnF889aQ3Hhhdt6E',
    requests: {
      now_playing: {
        name: 'now_playing',
        path: '3/movie/now_playing'
      },
      movie_genres: {
        name: 'movie_genres',
        path: '3/genre/movie/list'
      },
      get_image: {
        name: 'get_image',
        path: 'https://image.tmdb.org/t/p'
      }
    }
  },
  
  // *** Any default values required go here
  defaults: {
    voteSelection: {
      range: [0, 10],
      defaultValue: 3
    }
  }
}

export default config;
