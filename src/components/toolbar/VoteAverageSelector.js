import React, {useState} from 'react';
import {useStateValue} from '../../state';
import actions from '../../state/actions';
import './toolbar.scss';

/**
 * Displays <select> control to modify vote average property in the store.
 * Generates values from input prop `range` (an array) in increments of 0.5,
 * and sets initial value from provided prop `defaultValue`
 * @param props
 * @returns {*}
 * @constructor
 */
const VoteAverageSelector = (props) => {
  
  const [, dispatch] = useStateValue();
  const [currValue, setCurrValue] = useState(props.defaultValue);
  
  /**
   * Build <option>s to populate the parent <select>
   * @returns {*[]}
   */
  const buildOptions = () => {
    
    // *** generate source array of ints from max value in range prop,
    // *** doubled (to allow for n.5) plus 1 for final (max) value
    const temp = Array.from(Array((props.range[1] * 2) + 1).keys());
    
    // *** output desired values within range e.g. [0, 0.5, 1, 1.5...]
    const values = temp.map((value) => {
      return value / 2;
    });
    
    return values.map((value) => {
      return (
          <option
              key={value}
              value={value.toString()}
          >
            {value}
          </option>
      )
    });
  }
  
  /**
   * update local state and dispatch to store
   * @param e {Event}
   */
  const onChange = (e) => {
    setCurrValue(e.target.value);
    dispatch({
          type: actions.VOTE_SELECTION_CHANGED,
          payload: e.target.value
        }
    );
  }
  
  return (
      <div className={'vote-average-selector'}>
        <h4 className={'vote-average-selector__title'}>Vote selecta</h4>
  
        <div className={'vote-average-selector__inner'}>
          <select
              className={'select-box'}
              value={currValue}
              onChange={onChange}>
            {buildOptions()}
          </select>
        </div>
      </div>
  )
}

export default VoteAverageSelector;
