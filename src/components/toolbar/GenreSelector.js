import React, {useState} from 'react';
import {useStateValue} from '../../state';
import actions from '../../state/actions';
import './toolbar.scss';

/**
 * Displays <GenreTag> children
 * @param props
 * @returns {*}
 * @constructor
 */
const GenreSelector = (props) => {
  
  const [selectedItems, setSelectedItems] = useState([]);
  const [{availableGenres}, dispatch] = useStateValue();
  
  const onItemClicked = (id) => {
    updateSelections(id);
  }
  
  /**
   * Add or remove item from local state,
   * dispatch final selection state to the store
   * @param id {Number}
   */
  const updateSelections = (id) => {
    
    const itemExists = (selectedItems.indexOf(id) !== -1);
    let items = selectedItems;
    
    if (!itemExists) {
      items.push(id); // *** add item
    } else {
      items = items.filter((item) => item !== id); // *** remove item
    }
    setSelectedItems(items);
    dispatchData(items);
  }
  
  const dispatchData = (items) => {
    dispatch({
      type: actions.GENRE_SELECTION_CHANGED,
      payload: items
    })
  }
  
  /**
   * determine if item is selected or not,
   * in order to set style property
   * @param id {Number}
   * @returns {*}
   */
  const setItemIsSelectedState = (id) => {
    return selectedItems.find((genreId) => {
      return genreId === id;
    });
  }
  
  /**
   * determine if item is actually available for selection.
   * If not set style to grey the item out
   * @param id
   * @returns {*}
   */
  const setItemIsDisabledState = (id) => {
    
    if (!availableGenres) {
      return;
    }
    
    return availableGenres.find((genreId) => {
      return genreId === id;
    });
  }
  
  const {items} = props;
  
  return (
      <div className={'genre-selector'}>
        
        <h4 className={'genre-selector__title'}>Genre selecta</h4>
        
        <div className="genre-selector__inner">
        {items.map((item) => {
          return (
              <GenreTag
                key={item.id}
                id={item.id}
                label={item.name}
                onClicked={onItemClicked}
                isSelected={setItemIsSelectedState(item.id)}
                isDisabled={!setItemIsDisabledState(item.id)}
              />
          )
        })}
        </div>
      </div>
  )
}

/**
 * Child sub-component - the concrete 'button' item
 * @param props
 * @returns {*}
 * @constructor
 */
const GenreTag = (props) => {
  return (
      <div
          className={
            `genre-selector__item
            ${props.isSelected ?  'genre-selector__item--is-selected' : ''}
            ${props.isDisabled ?  'genre-selector__item--is-disabled' : ''}`
          }
          onClick={() => props.onClicked(props.id)}
      >
        {props.label}
      </div>
  )
}

export default GenreSelector;
