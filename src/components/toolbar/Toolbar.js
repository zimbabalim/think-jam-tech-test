import React from 'react';
import {useStateValue} from '../../state';
import GenreSelector from './GenreSelector';
import VoteAverageSelector from './VoteAverageSelector';
import config from '../../config';
import './toolbar.scss';

/**
 * Displays sub-components: <GenreSelector>, <VoteAverageSelector>, and info text
 * @param props
 * @returns {*}
 * @constructor
 */
const Toolbar = (props) => {
  
  const [{movieGenresData, filteredResults}, ] = useStateValue();
  
  return (
      <div className={'toolbar'}>
        <h1 className={'toolbar__title'}>Think Jam <span>movie selecta</span></h1>
        
        <div className="toolbar__inner">
        {movieGenresData && (
            <GenreSelector
                items={movieGenresData}
            />
        )}
        
        <VoteAverageSelector
          range={config.defaults.voteSelection.range}
          defaultValue={config.defaults.voteSelection.defaultValue}
        />
        
        </div>
  
        <div className={'toolbar__info'}>
        {filteredResults && filteredResults.length === 0
            ?
            <div>No results, please try modifying your selectas</div>
            :
            <div>Num results: <span>{filteredResults && filteredResults.length}</span></div>
        }
        </div>
        
      </div>
  )
}

export default Toolbar;
