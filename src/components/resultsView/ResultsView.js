import React from 'react';
import {useStateValue} from '../../state';
import ResultsItem from './ResultsItem';
import './results.scss';

/**
 * Displays movie items, parent of <ResultsItem> components
 * @param props
 * @returns {*}
 * @constructor
 */
const ResultsView = () => {
  
  const [{filteredResults, movieGenresData, genreSelections}, ] = useStateValue();
  
  /**
   * populate <ResultsItem> prop to feed in genre objects from the store
   * ids are ints that need to map to screen names
   * @param ids {Array}
   * @returns {[]}
   */
  const getGenres = (ids) => {
    
    if (!movieGenresData) {
      return null;
    }
    
    const results = [];
    
    ids.map((id) => { // *** match each input id...
      movieGenresData.find((item) => { // *** ...against store source object
        if (item.id === id) {
          results.push(item);
        }
        return null;
      });
      return null;
    });
    
    return results;
  }
  
  return (
      <div className={'results-view'}>
        <div className={'results-view__inner'}>
          {filteredResults && filteredResults.map((item) => {
            return (
                <ResultsItem
                    key={item.id}
                    title={item.title}
                    image={item.poster_path}
                    genres={getGenres(item.genre_ids) || []}
                    selectedGenres={genreSelections}
                />
            )
          })}
        </div>
      </div>
  )
}

export default ResultsView;
