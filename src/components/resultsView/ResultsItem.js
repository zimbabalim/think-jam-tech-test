import React from 'react';
import config from '../../config';
import './results.scss';

/**
 * Display movie item 'tile' - contains title, image and list of genres
 * Genres tags respond to store selections via `selectedGenres` prop
 * @param props
 * @returns {*}
 * @constructor
 */
const ResultsItem = (props) => {
  
  /**
   * load poster image
   * @returns {string}
   */
  const getImage = () => {
    
    const size = 'w500'; // *** NOTE improvement: determine viewport to load appropriate size image
    return `${config.api.requests.get_image.path}/${size}/${image}`;
  };
  
  /**
   * determine whether to highlight genre tag against store selection
   * @param id {Number}
   * @returns {boolean}
   */
  const highlightGenre = (id) => {

    let r = false;
    props.selectedGenres.map((item) => {
      if (item === id) {
        r = true;
      }
      return null;
    });
    
    return r;
  }
  
  const {title, image, genres} = props;
  
  return (
      <div className={'results-item'}>
        <h3 className={'results-item__title'}>{title}</h3>
        
        <div className={'results-item__image-wrapper'}>
          <img src={getImage(image)} alt={title} className={'results-item__image'}/>
        </div>
        
        <div className={'results-item__genres'}>
          {genres.map((item, index) => {
            return (
                <div
                    className={`genre-item ${highlightGenre(item.id) ? `genre-item--is-highlighted` : ''}`}
                    key={`genre_${index}`}
                >
                  {item.name}
                </div>
            )
          })}
        </div>
      </div>
  )
}

export default ResultsItem;
