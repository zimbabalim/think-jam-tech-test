# Think Jam tech test
### Aladin Scott - 14th April 2020


Project consumes data from [The Movie DB](https://www.themoviedb.org/settings/api), and allows the user to apply filtering on the list of available titles. Full project brief [here](https://docs.google.com/document/d/1JIXHo3tJpF5eQswsXTUMjvnD6R-Jzcb8go0XmL2RcHs/edit#).

### Build and run

I'm running Node v13.11.0 via `nvm`

To build run `npm i`

To start run `yarn start`

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Methodology

Follows typical React functional component approach, utilising `hooks` and `context`. Data flow is implemented via `context`, in a similar pattern to flux/redux. Redux would be overkill for this project, so I opted for this approach as it does not require an additional library.

The single reducer offloads processing to `state/utils` which is a small library of utilities as pure functions.

I've kept connections to `state` a responsibility of parent components in order to keep their children as simple as possible. 

Various settings are available in `config`, i.e. api keys and paths. Also defaults for controls. In this case the `vote average` property has a range and a default value set in `config`. This feeds the ui component `<VoteAverageSelector>` as well as the default value of the state property `currVoteSelection` - ensuring that the app filters the data against the correct values immediately as the data is available via network request.

There are no unit tests, but for a production ready app of this size I would implement tests for the business logic and network handling. I've also omitted much in the way of defensive programming - for production I would ensure all critical methods are able to break gracefully. I have thoroughly manually tested the app however.

Uses Sass for styling with most `scss` placed within each component's directory. The app is fully responsive, and there is little in the way of magic numbers in the styling - much of the look of the app can be transformed via values within `scss/variables`.
